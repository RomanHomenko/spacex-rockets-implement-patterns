//
//  MainView.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 24.04.2022.
//

import UIKit

class MainView: UIView {
    // MARK: - Full and detail info about Rocket
    @IBOutlet var detailContentView: UIView!
    
    // MARK: - Rocket name
    @IBOutlet var rocketNameLabel: UILabel!
    
    // MARK: - Shoud use to popUp settings later
    @IBOutlet var settingsButton: UIButton!
    
    // MARK: - Random rocketImage
    // use Bundle to get access for assets later
    @IBOutlet var rocketImageView: UIImageView!
    
    // MARK: - CollectionView instance
    @IBOutlet var rocketParametrsCollectionView: UICollectionView!
    
    // MARK: - Info about Rockets' Past
    @IBOutlet var dateLaunchLabel: UILabel!
    @IBOutlet var launchCountryLabel: UILabel!
    @IBOutlet var costLaunchLabel: UILabel!
    
    
    // MARK: Info First Step Labels
    @IBOutlet var firstNumberOfPartsLabel: UILabel!
    @IBOutlet var firstFuelQuantityLabel: UILabel!
    @IBOutlet var firstTimeToBurnLabel: UILabel!
    
    // MARK: Info Second Step Labels
    @IBOutlet var secondNumberOfPartsLabel: UILabel!
    @IBOutlet var secondFuelQuantityLabel: UILabel!
    @IBOutlet var secondTimeToBurnLabel: UILabel!
    
    // MARK: - Button to see all rocket launches
    @IBOutlet var rocketLaunchesButton: UIButton!
}
