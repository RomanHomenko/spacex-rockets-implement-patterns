//
//  RocketLaunchCell.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 15.04.2022.
//

import UIKit

// MARK: - Model for cell in DetailViewController
class RocketLaunchCell: UICollectionViewCell {
    @IBOutlet var rocketNameLabel: UILabel!
    @IBOutlet var dateLaunchLabel: UILabel!
    @IBOutlet var successfulLaunch: UIImageView!
}
