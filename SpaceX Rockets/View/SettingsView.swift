//
//  SettingsView.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 24.04.2022.
//

import UIKit

class SettingsView: UIView {
    @IBOutlet var heightSegmet: UISegmentedControl!
    @IBOutlet var diameterSegment: UISegmentedControl!
    @IBOutlet var massSegment: UISegmentedControl!
    @IBOutlet var payloadWeightsSegment: UISegmentedControl!
}
