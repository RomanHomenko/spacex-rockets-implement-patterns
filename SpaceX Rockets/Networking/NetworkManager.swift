//
//  NetworkManager.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 14.04.2022.
//

import Foundation
import UIKit

class NetworkManager {
    static let shared = NetworkManager()
    // MARK: - generic Fetch function to fetch RocketData and RocketInSpaceData from API
    func fetchRocketData<T: Decodable>(from url: String, completion: @escaping (T?, Error?) -> Void) {
        
        guard let url = URL(string: url) else { return }
        
        var request = URLRequest(url: url, timeoutInterval: Double.infinity)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                guard let data = data else {
                    print(String(describing:error))
                    completion(nil, error)
                    return
                }
                do {
                    let ojects = try JSONDecoder().decode(T.self, from: data)
                    completion(ojects, nil)
                } catch let jsonError {
                    print(String(describing: jsonError))
                    completion(nil, jsonError)
                }
            }
        }
        
        task.resume()
    }
    
    func feetchImageFromURL(urlAdress: String, completion: @escaping (UIImage?) -> Void) {
        guard let url = URL(string: urlAdress) else {
            return
        }
        if let imageData = try? Data(contentsOf: url) {
            if let loadImage = UIImage(data: imageData) {
                DispatchQueue.main.async {
                    completion(loadImage)
                }
            }
        }
    }
    
    private init() {}
}
