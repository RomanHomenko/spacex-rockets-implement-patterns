//
//  Constants.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 14.04.2022.
//

import Foundation

// SpaceX API's URL
let rocketsDataURLString = "https://api.spacexdata.com/v4/rockets"
let rocketsLounchesURLString = "https://api.spacexdata.com/v4/launches"

// UserDefaults keys for measureements
enum Keys: String, CaseIterable {
    case height = "height"
    case diameter = "diameter"
    case mass = "mass"
    case payloadWeights = "payloadWeights"
}

// UserDefaults keys for segmentControls
enum IndexSegmentKeys: String, CaseIterable {
    case heightIndex = "heightIndex"
    case diameterIndex = "diameterIndex"
    case massIndex = "massIndex"
    case payloadWeightsIndex = "payloadWeightsIndex"
}

// Measurements
enum Measurements: String {
    case height = "height"
    case diameter = "diameter"
    case mass = "mass"
    case payloadWeights = "payloadWeights"
}

// Segue Identifiers
enum Identifiers: String {
    case rocketLaunches
    case settings
}

var keyRocketParameters = "rocketParameters"
var keyRocketParametersIndex = "rocketParametersIndex"
