//
//  UserDefaultsManager.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 17.04.2022.
//

import Foundation

struct UserDefaultsManager {
    static let shared = UserDefaultsManager()
    
    let defaults = UserDefaults.standard
    let jsonDecoder = JSONDecoder()
    let jsonEncoder = JSONEncoder()
    
    func fetchMeasurements(completion: @escaping (RocketParameters?, Error?) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            if let jsonData = defaults.object(forKey: keyRocketParameters) as? Data {
                do {
                    completion(try jsonDecoder.decode(RocketParameters.self, from: jsonData), nil)
                } catch let error {
                    completion(nil, error)
                }
            }
        }
    }
    
    func fetchIndexesFromUserDefaults(completion: @escaping (RocketParametersIndex?, Error?) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
//            for key in IndexSegmentKeys.allCases {
                if let jsonData = defaults.object(forKey: keyRocketParametersIndex) as? Data {
                    DispatchQueue.main.async {
                        do {
                            completion(try jsonDecoder.decode(RocketParametersIndex.self, from: jsonData), nil)
                        } catch let error {
                            completion(nil, error)
                        }
                    }
                }
//            }
        }
    }
    
    func saveInUserDefaults<T: Encodable>(element: T, key: String) {
        if let savedData = try? jsonEncoder.encode(element) {
            defaults.set(savedData, forKey: key)
        }
    }
    
    private init() {}
}
