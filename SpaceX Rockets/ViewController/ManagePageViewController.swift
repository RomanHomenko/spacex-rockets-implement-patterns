//
//  ManagePageViewController.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 14.04.2022.
//

import UIKit

class ManagePageViewController: UIPageViewController {
    var currentIndex: Int!
    var rockets: [RocketData]?
    
    // MARK: - managers (singletone)
    let networkManager: NetworkManager = NetworkManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let viewController = viewRocketInfoController(currentIndex ?? 0) {
            let viewControllers = [viewController]
            
            setViewControllers(viewControllers, direction: .forward, animated: false)
        }
        
        dataSource = self
    }
}

// MARK: - Create VCs and fill it with data
extension ManagePageViewController {
    func viewRocketInfoController(_ index: Int) -> ViewController? {
        guard let storyboard = storyboard,
              let page = storyboard
            .instantiateViewController(withIdentifier: "ViewController") as? ViewController else { return nil }
        
        networkManager.fetchRocketData(from: rocketsDataURLString) { [weak self] (rocketsResponse: [RocketData]?, error) in
            rocketsResponse.map { [weak self] rockets in
                let rocket = rockets[index]
                
                var formattedData: String {
                    let dateFormatterGet = DateFormatter()
                    dateFormatterGet.dateFormat = "yyyy-mm-dd"
                    
                    let dateFormatterPrint = DateFormatter()
                    dateFormatterPrint.dateFormat = "dd MMM, yyyy"
                    
                    let date: Date? = dateFormatterGet.date(from: rocket.firstFlight)
                    
                    return String(describing: dateFormatterPrint.string(from: date!))
                }
                
                self?.rockets = rockets
                page.rocket = rockets[index]
                page.pageIndex = index
                
                page.mainView.rocketNameLabel.text = rocket.name
                page.mainView.dateLaunchLabel.text = formattedData
                
                if rocket.country == "United States" {
                    page.mainView.launchCountryLabel.text = "США"
                } else {
                    page.mainView.launchCountryLabel.text = "М. Острова"
                }
                
                page.mainView.costLaunchLabel.text = "$\(rocket.costPerLaunch / 1_000_000) млн"
                
                page.mainView.firstNumberOfPartsLabel.text = "\(rocket.firstStage.engines)"
                page.mainView.firstFuelQuantityLabel.text = "\(rocket.firstStage.fuelAmountTons)"
                page.mainView.firstTimeToBurnLabel.text = "\(rocket.firstStage.burnTimeSEC ?? 0)"
                
                page.mainView.secondNumberOfPartsLabel.text = "\(rocket.secondStage.engines)"
                page.mainView.secondFuelQuantityLabel.text = "\(rocket.secondStage.fuelAmountTons)"
                page.mainView.secondTimeToBurnLabel.text = "\(rocket.secondStage.burnTimeSEC ?? 0)"
                
                page.mainView.rocketParametrsCollectionView.reloadData()
                }
        }
        
        return page
    }

}

// MARK: - Implement funcs for PageViewControllerDataSource 
extension ManagePageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let viewController = viewController as? ViewController,
           let index = viewController.pageIndex, index > 0 {
            return viewRocketInfoController(index - 1)
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let viewController = viewController as? ViewController,
           let index = viewController.pageIndex,
           (index + 1) < rockets?.count ?? 0 {
            return viewRocketInfoController(index + 1)
        }
        
        return nil
    }
}


// MARK: - Displaying a Page Control Indicator
extension ManagePageViewController {
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return 4
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return currentIndex ?? 0
    }
}
