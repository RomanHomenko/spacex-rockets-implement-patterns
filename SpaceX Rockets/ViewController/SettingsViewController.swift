//
//  SettingsViewController.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 16.04.2022.
//

import UIKit

class SettingsViewController: UIViewController {
    
    // MARK: - All VIEWs here
    weak var settingsView: SettingsView! {
        guard isViewLoaded else { return nil }
        return (view as! SettingsView)
    }
    
    // MARK: - Rocket measurements
    var currentRocketParameters = RocketParameters()
    
    // MARK: - Index of rocket parameteers in segment control
    var rocketParametersIndex = RocketParametersIndex()

    weak var delegate: ViewControllerDelegate?
    
    let vibration = HapticManager.shared
    
    // MARK: - UserDefaults (singletone)
    let userDefaultsManager = UserDefaultsManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingsView.heightSegmet.addTarget(self, action: #selector(changeMeasurements), for: .valueChanged)
        settingsView.diameterSegment.addTarget(self, action: #selector(changeMeasurements), for: .valueChanged)
        settingsView.massSegment.addTarget(self, action: #selector(changeMeasurements), for: .valueChanged)
        settingsView.payloadWeightsSegment.addTarget(self, action: #selector(changeMeasurements), for: .valueChanged)
    }
    
    // MARK: - reload collection view in main ViewController by delegate pattern
    @IBAction func dismissViewControllerButton(_ sender: UIButton) {
        delegate?.reloadRocketInfo()
        
        vibration.selectionVibrate()
        
        dismiss(animated: true)
    }
    
    // MARK: - activate vibration after change segment
    @IBAction func activateVibrationTapped(_ sender: Any) {
        vibration.selectionVibrate()
    }
}

// MARK: - Change measurements when segmentControl value is change and save segmentControle in UseerDefaults
extension SettingsViewController {
    @objc func changeMeasurements(target: UISegmentedControl) {
        let selectedIndex = target.selectedSegmentIndex
        switch target {
        case settingsView.heightSegmet:
            setMeasurements(for: Measurements.height.rawValue, target.titleForSegment(at: selectedIndex) ?? "")
            rocketParametersIndex.heighIndext = selectedIndex
        case settingsView.diameterSegment:
            setMeasurements(for: Measurements.diameter.rawValue, target.titleForSegment(at: selectedIndex) ?? "")
            rocketParametersIndex.diameterIndex = selectedIndex
        case settingsView.massSegment:
            setMeasurements(for: Measurements.mass.rawValue, target.titleForSegment(at: selectedIndex) ?? "")
            rocketParametersIndex.massIndex = selectedIndex
        case settingsView.payloadWeightsSegment:
            setMeasurements(for: Measurements.payloadWeights.rawValue, target.titleForSegment(at: selectedIndex) ?? "")
            rocketParametersIndex.payloadWeightsIndex = selectedIndex
        default:
            break
        }
        userDefaultsManager.saveInUserDefaults(element: rocketParametersIndex, key: keyRocketParametersIndex)
    }
}

// MARK: - Set value measurements and save it to UserDefaults
extension SettingsViewController {
    func setMeasurements(for parametr: String,_ measurement: String) {
        switch parametr {
        case Measurements.height.rawValue:
            currentRocketParameters.height = measurement
            
        case Measurements.diameter.rawValue:
            currentRocketParameters.diameter = measurement
            
        case Measurements.mass.rawValue:
            currentRocketParameters.mass = measurement
            
        case Measurements.payloadWeights.rawValue:
            currentRocketParameters.payloadWeights = measurement
        default:
            break
        }
        userDefaultsManager.saveInUserDefaults(element: currentRocketParameters, key: keyRocketParameters)
    }
}


// MARK: - Fetch segmentControls indexes from UserDefaults when viewWillAppear
extension SettingsViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        userDefaultsManager.fetchIndexesFromUserDefaults { [weak self] rocketIndexes, error in
            if let error = error {
                print(String(describing: error))
                return
            }
            
            self?.rocketParametersIndex = rocketIndexes ?? RocketParametersIndex()
            self?.settingsView.heightSegmet.selectedSegmentIndex = rocketIndexes?.heighIndext ?? 0
            self?.settingsView.diameterSegment.selectedSegmentIndex = rocketIndexes?.diameterIndex ?? 0
            self?.settingsView.massSegment.selectedSegmentIndex = rocketIndexes?.massIndex ?? 0
            self?.settingsView.payloadWeightsSegment.selectedSegmentIndex = rocketIndexes?.payloadWeightsIndex ?? 0
        }
        
        userDefaultsManager.fetchMeasurements { fetchedRocketParameters, error in
            if let error = error {
                print(String(describing: error))
                return
            }
            
            self.currentRocketParameters.height = fetchedRocketParameters?.height ?? ""
            self.currentRocketParameters.diameter = fetchedRocketParameters?.diameter ?? ""
            self.currentRocketParameters.mass = fetchedRocketParameters?.mass ?? ""
            self.currentRocketParameters.payloadWeights = fetchedRocketParameters?.payloadWeights ?? ""
        }
    }
}
