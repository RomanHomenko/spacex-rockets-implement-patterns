//
//  ViewController.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 14.04.2022.
//

import UIKit
import UserNotifications

// MARK: - TODO
// [] - Add Alamofire to work with NetworkRequest

// [] - Think about filtering requests to APIs with alamofire (IMPORTANT)

// [x] - implement Singletone to Networking
// [x] - implement Memento to store data in UserDefaults [x] - (Can make it Singletone too)
// [x] - Update CollectionView every time after new data fetched in DetailVC
// [x] - Make project more clear MVC
// -> move View code into another file and "bind" it with storyboard
// [x] - Make VCs simplier by divide VCs loguc into separate files
// [x] - Simplify acces to UserDefaults (Maybe create one Class with 4 enums)
// [x] - replace hardoced Strings to write or fetch from UserDefaults with enums?

// [x] - Add some animations

// MARK: - Create delegate protocol
protocol ViewControllerDelegate: AnyObject {
    func reloadRocketInfo()
}

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
     
    // MARK: - All VIEWs here
    weak var mainView: MainView! {
        guard isViewLoaded else { return nil }
        return (view as! MainView)
    }
    
    // MARK: - One rocket from JSON to display
    var rocket: RocketData?
    
    // MARK: - Current index of Page
    var pageIndex: Int!
    
    // MARK: - Rocket measurements
    var rocketParameters = RocketParameters()
    
    // MARK: - Make haptic vibration
    let vibration = HapticManager.shared
    
    // MARK: - For interaction with notifications' functionality
    let notification = Notification()
    
    // MARK: - Fetch images (singletone)
    let networkManager = NetworkManager.shared
    
    // MARK: - Fetch measurements (singletone)
    let userDefaultsManager = UserDefaultsManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        downloadImageFromURL() // if image == nil -> Download else image fetch from UserDefaults
        fetchCurrentMeasurements()
        
        // seend notification
        notification.askPermissionToSendNotifications()
        notification.sendNotification()
        
        viewSettings()
    }
    
    // MARK: - activate haptic vibration
    @IBAction func hapticVibrationTapped(_ sender: Any) {
        vibration.selectionVibrate()
    }
    
}

extension ViewController {
    func viewSettings() {
        mainView.detailContentView.roundCorners([.topLeft, .topRight], radius: 45)
        mainView.dateLaunchLabel.sizeToFit()
        mainView.settingsButton.setTitle("", for: .normal)
        mainView.rocketLaunchesButton.layer.cornerRadius = 15
        navigationController?.isNavigationBarHidden = true
    }
}

// MARK: - Add protocol stubs for UICollectionViewDataSource and setting Cells
extension ViewController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! RocketParametrCell
        let tap = UITapGestureRecognizer(target: self, action: #selector(startAnimationCell(sender:)))
        
        // Create four different models for this
        switch indexPath.row {
        case 0:
            cell.metricLabel.text = "\(String(describing: rocketParameters.height=="m" ? rocket?.height.meters ?? 0 : rocket?.height.feet ?? 0))"
            cell.metricDescriptionLabel.text = "высота, \(rocketParameters.height)"
        case 1:
            cell.metricLabel.text = "\(String(describing: rocketParameters.diameter=="m" ? rocket?.diameter.meters ?? 0 : rocket?.diameter.feet ?? 0))"
            cell.metricDescriptionLabel.text = "диаметр, \(rocketParameters.diameter)"
        case 2:
            cell.metricLabel.text = "\(String(describing: rocketParameters.mass=="kg" ? rocket?.mass.kg ?? 0: rocket?.mass.lb ?? 0))"
            cell.metricDescriptionLabel.text = "масса, \(rocketParameters.mass)"
        case 3:
            cell.metricLabel.text = "\(String(describing: rocketParameters.payloadWeights=="kg" ? rocket?.payloadWeights.first?.kg ?? 0: rocket?.payloadWeights.first?.lb ?? 0))"
            cell.metricDescriptionLabel.text = "нагрузка, \(rocketParameters.payloadWeights)"
        default:
            break
        }
        
        cell.layer.cornerRadius = 40
        cell.addGestureRecognizer(tap)
        
        return cell
    }
            
    @objc func startAnimationCell(sender: UITapGestureRecognizer) {
        let tapLocation = sender.location(in: self.mainView.rocketParametrsCollectionView)
        if let indexPath = self.mainView.rocketParametrsCollectionView.indexPathForItem(at: tapLocation) {
            if let cell = self.mainView.rocketParametrsCollectionView.cellForItem(at: indexPath) as? RocketParametrCell {
                UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: []) {
                    cell.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                    self.vibration.selectionVibrate()
                } completion: { finished in
                    cell.transform = .identity
                }
            }
        }
    }
}

// MARK: - UIView extensions
extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}


// MARK: - Fetch Images from API's URL
extension ViewController {
    func downloadImageFromURL() {
        DispatchQueue.global(qos: .userInteractive).async { [weak self] in
            if self?.rocket == nil {
                self?.downloadImageFromURL()
            }
            self?.networkManager.feetchImageFromURL(urlAdress: self?.rocket?.flickrImages.first ?? "") { [weak self] image in
                DispatchQueue.main.async {
                    self?.mainView.rocketImageView.image = image
                }
            }
        }
    }
}

// MARK: - Work with segues
extension ViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Identifiers.rocketLaunches.rawValue {
            let rocketName = rocket?.name ?? ""
            let rocketID = rocket?.id ?? ""
    
            let detailVC = segue.destination as! DetailCollectionViewController
            detailVC.rocketName = rocketName
            detailVC.rocketID = rocketID
        }
        if segue.identifier == Identifiers.settings.rawValue {
            // Implement delegate pattern
            let settingsVC = segue.destination as! SettingsViewController
            settingsVC.delegate = self
        }
    }
}

extension ViewController {
    func fetchCurrentMeasurements() {
        userDefaultsManager.fetchMeasurements { [weak self] (fetchedRocketParameters, error) in
            self?.rocketParameters.height = fetchedRocketParameters?.height ?? ""
            self?.rocketParameters.diameter = fetchedRocketParameters?.diameter ?? ""
            self?.rocketParameters.mass = fetchedRocketParameters?.mass ?? ""
            self?.rocketParameters.payloadWeights = fetchedRocketParameters?.payloadWeights ?? ""
        }
    }
}

// MARK: - Implement delegate function
extension ViewController: ViewControllerDelegate {
    func reloadRocketInfo() {
        fetchCurrentMeasurements()

        mainView.rocketParametrsCollectionView.reloadData()
    }
}

// MARK: - reload VC's collectionView after turn page's VC
extension ViewController {
    override func viewWillAppear(_ animated: Bool) {
        reloadRocketInfo()
    }
}
