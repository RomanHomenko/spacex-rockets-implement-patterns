//
//  Notifications.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 17.04.2022.
//

import UserNotifications

// MARK: - Implement notification to remind user to use my app more often
struct Notification {
    let notificationCenter = UNUserNotificationCenter.current()
    
    func askPermissionToSendNotifications() {
        notificationCenter.requestAuthorization(options: [.alert, .sound]) { permission, error in
            guard permission else { return }
            self.notificationCenter.getNotificationSettings { settings in
                
                guard settings.authorizationStatus == .authorized  else { return }
            }
        }
    }
    
    // Will send every day to check new launches
    func sendNotification() {
        let content = UNMutableNotificationContent()
        content.title = "Time to chek new launches!"
        content.body = "Hurry up!"
        content.badge = NSNumber(value: 1)
        content.sound = UNNotificationSound.default
        
        var datComp = DateComponents()
        datComp.hour = 12
        datComp.minute = 0
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: datComp, repeats: true)
        
        let request = UNNotificationRequest(identifier: "notification", content: content, trigger: trigger)
        notificationCenter.add(request) { error in
            print(String(describing: error))
        }
    }
}

