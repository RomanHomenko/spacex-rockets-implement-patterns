//
//  RocketParameters.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 25.04.2022.
//

import Foundation

struct RocketParameters: Codable {
    var height: String = "m"
    var diameter: String = "m"
    var mass: String = "kg"
    var payloadWeights: String = "kg"
}

struct RocketParametersIndex: Codable {
    var heighIndext: Int = 0
    var diameterIndex: Int = 0
    var massIndex: Int = 0
    var payloadWeightsIndex: Int = 0
}
