//
//  RocketInSpaceData.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 15.04.2022.
//

import Foundation

class RocketInSpaceData: Decodable {
    let name: String
    let success: Bool?
    let dateUTC: String
    let rocket: Rocket

    enum CodingKeys: String, CodingKey {
        case name
        case dateUTC = "date_utc"
        case success
        case rocket
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decode(String.self, forKey: .name)
        success = try values.decode(Bool?.self, forKey: .success)
        dateUTC = try values.decode(String.self, forKey: .dateUTC)
        rocket = try values.decode(Rocket.self, forKey: .rocket)
    }
}

enum Rocket: String, Codable {
    case the5E9D0D95Eda69955F709D1Eb = "5e9d0d95eda69955f709d1eb"
    case the5E9D0D95Eda69973A809D1Ec = "5e9d0d95eda69973a809d1ec"
    case the5E9D0D95Eda69974Db09D1Ed = "5e9d0d95eda69974db09d1ed"
}
